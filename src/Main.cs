﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace CECarryWeightPatch
{
	[StaticConstructorOnStartup]
	public static class Main
    {
		static Main()
		{
			var harmony = new Harmony("RW_CE_Carry_Patch");
			harmony.PatchAll(Assembly.GetExecutingAssembly());
		}
    }
}
