﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using UnityEngine;

namespace CECarryWeightPatch
{
	[HarmonyPatch(typeof(MassUtility), nameof(MassUtility.Capacity))]
	public static class MassUtilityPatch
	{
		[HarmonyPostfix]
		public static void Postfix(Pawn p, ref float __result)
		{
			if (__result <= 0f)
				return;

			float baseCarryingCapacity = p.def.statBases.GetStatValueFromList(StatDefOf.CarryingCapacity, 75f);
			__result += Mathf.Max(p.GetStatValue(StatDefOf.CarryingCapacity) - baseCarryingCapacity, 0f);
		}
	}
}
