﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CombatExtended;
using HarmonyLib;
using RimWorld;
using Verse;
using UnityEngine;

namespace CECarryWeightPatch
{
	[HarmonyPatch(typeof(CompInventory), nameof(CompInventory.capacityWeight), MethodType.Getter)]
	internal static class CapacityWeightPatch
	{
		[HarmonyPostfix]
		static void Postfix(Pawn ___parentPawnInt, ref float __result)
		{
			if (___parentPawnInt == null) return;

			float baseCarryingCapacity = ___parentPawnInt.def.statBases.GetStatValueFromList(StatDefOf.CarryingCapacity, 75f);
			__result += Mathf.Max(___parentPawnInt.GetStatValue(StatDefOf.CarryingCapacity) - baseCarryingCapacity, 0f);
		}
	}

	[HarmonyPatch(typeof(CompInventory), nameof(CompInventory.capacityBulk), MethodType.Getter)]
	internal static class CapacityBulkPatch
	{
		[HarmonyPostfix]
		static void Postfix(Pawn ___parentPawnInt, ref float __result)
		{
			if (___parentPawnInt == null) return;

			float baseCarryingCapacity = ___parentPawnInt.def.statBases.GetStatValueFromList(StatDefOf.CarryingCapacity, 75f);
			__result += Mathf.Max(___parentPawnInt.GetStatValue(StatDefOf.CarryingCapacity) - baseCarryingCapacity, 0f);
		}
	}
}
